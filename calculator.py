"""
Calculator library containing basic math operations.
"""


def add(first_value, second_value):
    return first_value + second_value


def subtract(first_value, second_value):
    return first_value - second_value


def multiply(first_value, second_value):
    return first_value * second_value


def divide(first_value, second_value):
    return first_value / second_value
