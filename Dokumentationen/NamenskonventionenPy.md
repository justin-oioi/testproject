## Namenskonventionen in Python

### Vorwort
Naiver Weise dachte ich bis vor kurzem, dass die Konventionen, die ich für Java damals gelernt habe generell für den ganzen Programmierbereich galten und auch eher zur Klarheit da waren und nicht für essentielle Bestandteile des Codes. Nachdem ich nun Issue #2 gesehen hatte ist mir bewusst geworden wie unlogisch das eigentlich ist. Lange Rede kurzer Sinn, ich hab mich schlauer gemacht.

### Namenskonvention für pytest
Wie schon im Issue erwähnt lässt pytest nur Dateien durchlaufen die entweder test_*.py oder *_test.py heißen. Das * steht hier, wie fast überall, für beliebig (Also Wörter oder Zeichen sprich schreiben was man möchte).

### Klassen
Klassen in Python werden nach dem PascalCase geschrieben, das heißt alles wird zusammen geschrieben und jedes Wort, auch der erste Buchstabe, wird großgeschrieben (Wegen Java kannte ich hier z.B. nur den camelCase).

### Variablen, Funktionen und Methoden
Standardmäßige Variablen, Funktionen und Methoden folgen dem snake_case, das heißt alles klein und einzelne Wörter werden mit Unterstrichen getrennt. Außerdem kann man hier vor dem eigentlichen Namen einen Unterstrich für protected oder zwei für private benutzen um deren Zugriff zu markieren. Python selbst hat eigentlich jedoch kein Zugriffsmanagement und somit ist das lediglich eine symbolische Konvention (Laut meiner Recherche, es gab jedoch auch Seiten die gesagt haben es wirft Fehler auf).

### Anderes
- Paket-, Modul-, und Dateinamen - snake_case
- Globale Variablen - UPPER_CASE_SNAKE_CASE
- Proprietäre Variablen - 2 Unterstriche am Anfang, alles klein dann 2 Unterstriche am Ende
- Konstanten - UPPER_CASE_SNAKE_CASE
