## Test Driven Development

### Vorwort
In Verbindung zu Issue #3 eine kleine Dokumentation was Test Driven Development (TDD) ist und was ich gemacht hab bei diesem Issue.

### Was ist TDD?
Test Driven Development ist eine Methode die häufig bei agilen Entwicklungen ihren Platz findet, also ideal für DevOps. Bei dieser Methode werden konsequent Tests vor den eigentlichen Methoden/Komponenten geschrieben. Es gibt einige Gründe für die Einführung solch einer Methode:
- Fehlende oder mangelnde Testbarkeit des Systems (monolithisch, Nutzung von Fremdkomponenten, …).
- Verbot der Investition in nicht-funktionale Programmteile seitens der Unternehmensführung. („Arbeit, von der man später im Programm nichts sieht, ist vergeudet.“)
- Erstellung von Tests unter Zeitdruck.
- Nachlässigkeit und mangelnde Disziplin der Programmierer bei der Testerstellung.
<br>*Quelle: https://de.wikipedia.org/wiki/Testgetriebene_Entwicklung*

Diese Methode soll also dafür sorgen, dass Tests ordentlich geschrieben werden.

### Was habe ich gemacht
Passend zur Methode habe ich die Tests zu erst geschrieben, was wenig Arbeit war. Dabei habe ich mich trotzdem noch über Tests in Python schreiben schlau gemacht (zur Info über assert, usw.). Tests waren dann fertig, Methoden passend noch eingefügt, kleine Anpassung und eigentlich war das Issue dann schon erledigt. Das Problem war anschließend dass der eigentliche GitLab-Runner keinen Speicherplatz mehr hatte und da ich eine feste Größe eingestellt hatte, konnte dieser nicht mehr erweitert werden, somit habe ich einen komplett neuen Runner aufgesetzt wodurch ich ein paar Befehle in der anderen Dokumentation nochmal berichtigen konnte, eine kleine Veränderung in der CI und dann ging alles wieder.
