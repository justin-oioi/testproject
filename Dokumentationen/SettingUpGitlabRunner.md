## Wie setzt man einen GitLab-Runner für sein Projekt auf? (Linux OS)

### Vorwort
Ich habe mich für eine VM mit Linux (in dem Fall Ubuntu) Umgebung entschieden um möglichen Windows Problemen auszuweichen, da der Runner Job von einem Runner unter Linux ausgeführt wurde.

### Installieren des GitLab-Runners
GitLab hat dazu eine ausführliche Dokumentation geschrieben: https://docs.gitlab.com/runner/install/linux-manually.html

Kurzum, zuerst sucht man eine passende Version für sein OS raus für Linux gibt es viele mögliche Varianten zum Download z.B. für Debian, Ubuntu, CentOS, usw.

Ich habe Ubuntu installiert deshalb habe ich den in der GitLab Dokumentation gezeigten Befehl benutzt mit der amd64 Architektur für Download und Installation, was das ist weiß ich nicht. Eine kurze Google Suche würde das wahrscheinlich erklären aber ich wollte mich nicht an den Kleinigkeiten aufhalten.

Download:

`curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"`

Für diesen Befehl musste ich vorher noch curl installieren:

`sudo apt install curl`

Installieren:

`sudo apt install git`
`sudo dpkg -i gitlab-runner_amd64.deb`

Beim registrieren habe ich docker als Umgebung für den Runner ausgewählt und als Standardimage python:3 ausgewählt, da dies ein Python-Projekt ist. Die Registration startet man über den Befehl:

`sudo gitlab-runner register`

Danach habe ich docker installiert über:

`sudo apt install docker.io`

Damit der Runner dann aber ohne Probleme laufen konnte musste der Docker Service noch gestartet werden über:

`systemctl start docker`

Anschließend habe ich einen Pipeline Job gestartet und nach 3 Minuten hatte der Runner das Python-Image und durchlief den Job ohne Probleme.
