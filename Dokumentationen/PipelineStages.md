## Stages in einer Pipeline

### Was ist eine Stage
Eine Stage ist ein Schritt in der Pipeline in der bestimmte Sachen ausgeführt werden, wie sie heißen und was sie tun ist einem selber überlassen. Stages sorgen für logische Unterteilungen und Debugging bei Fehlern. Mehrere Stages sollten immer in Erwägung gezogen werden für eine bessere Projektübersicht.

### Was habe ich gemacht
Ich habe mich dazu entschlossen zusätzlich zu der "test" Stage eine "build" Stage hinzuzufügen. In dieser wird ein Ordner erstellt mit der Datei "requirements.txt", anschließend sollen die Vorraussetzungen des Runners darin gespeichert werden und als Artifact für spätere Durchläufe erhalten bleiben.

Zur Hilfe habe ich [dieses Video](https://www.youtube.com/watch?v=Jav4vbUrqII) benutzt.
